package main

import (
	"github.com/julienschmidt/httprouter"
    "../models/dial" 
    "./auth" 
    "./poll" 
    "log"
    "net/http"
)

func main() {
// helping-servers setup
    
    dial.Setup_redis();
    defer dial.Close_redis()

    dial.Setup_sql();
    defer dial.Close_sql()

// helping-servers set

    router := httprouter.New()
    router.GET("/", auth.Index)
    router.POST("/login", auth.Login)
    router.GET("/paper", poll.Paper)
    router.POST("/ballot", poll.Vote)
    router.GET("/logout", auth.Logout)
    log.Fatal(http.ListenAndServe(":8080", router))
}