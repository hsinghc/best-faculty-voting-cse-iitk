package poll

import (
 "net/http"
 SC "../../conf/server_conf" 
 EC "../../conf/election_conf" 
 "github.com/julienschmidt/httprouter"
 "../../models/model"
 //"encoding/json"
 "strings"
 "strconv"
 "html/template"
 "fmt"

)






func Vote(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	//decoder := json.NewDecoder(r.Body)
    var cookie string
    cookies := r.Cookies()
    for _,value:= range cookies{
        if value.Name=="ACAVote" {
            cookie = value.Value
            break;
        }
    }
    guard := model.Check_logged_in(cookie)
    if guard {
        http.Redirect(w,r,"/",302)
        return
    }

	User := model.Electorate_Profile{}
    User.Cookie = cookie
    //User.Votes = r.FormValue("Votes")
	/*err := decoder.Decode(&User)
	if err!=nil {
        http.Error(w,"Internal_Server_Error",500)
        return
    }*/
    for i := 0; i < EC.Number_of_votes; i++ {
        User.Votes=append(User.Votes,r.FormValue(strconv.Itoa(i+1)))
    }

    guard = User.Validate()
    if guard {
        http.Redirect(w,r,"/ballot",302)
        return
    	// not logged in redirect to auth
    }
    fmt.Println("Validated Votes")
    s := strings.Split(User.Cookie, "@")
    username := s[0]
    category := s[1]
   
   for i := 0; i < EC.Number_of_votes; i++ {
    	 
        stmt, err := SC.Sqldb.Prepare("INSERT INTO Ballot (username,category,Votes_"+strconv.Itoa(i+1)+") VALUES (\""+username+"\""+","+"\""+category+"\""+","+"\""+User.Votes[i]+"\""+") ON DUPLICATE KEY UPDATE Votes_"+strconv.Itoa(i+1)+"=\"" + User.Votes[i] +"\"")
    	if err != nil {
            panic(err.Error()) 
        }  
        fmt.Println("err1",stmt) 
        g, err2 := stmt.Exec()
        if err2 != nil {
            panic(err.Error()) // proper error handling instead of panic in your app
        }
        fmt.Println("err2",g) 
   }

    http.Redirect(w,r,"/paper",302)

    //render
    return


}
func Paper(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
    var cookie string
    cookies := r.Cookies()
    for _,value:= range cookies{
        if value.Name=="ACAVote" {
            cookie = value.Value
            break;
        }
    }
    guard := model.Check_logged_in(cookie)
    if guard {
        http.Redirect(w,r,"/",302)
        return
    }else{
        s := strings.Split(cookie, "@")
        username := s[0]
        category := s[1]
        current_votes := make([]string, EC.Number_of_votes, EC.Number_of_votes)
        for i := 0; i < EC.Number_of_votes; i++ {
            if err := SC.Sqldb.QueryRow("SELECT Votes_"+strconv.Itoa(i+1)+" FROM Ballot WHERE username=\"" + username +"\"" + " AND category=\""+category+"\"" ).Scan(&current_votes[i]); (err != nil) && (err != SC.SqlErrNoRows) {
                panic(err.Error()) 
            }
        }
        fmt.Println("CurreAAAAAAAAAAAAAAAAaa",current_votes)
        t, _ := template.ParseFiles("/media/harpreet/winstuff/ACA/Best_Faculty_Voting/src/views/poll.html")
        t.Execute(w, current_votes )
    }

}