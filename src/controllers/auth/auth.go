package auth

import (
 SC "../../conf/server_conf" 
 "../../models/model"
 //"encoding/json"
 "github.com/dutchcoders/goftp"
 "net/http"
 "github.com/julienschmidt/httprouter"
 "html/template"
 "fmt"
)


func login_from_server(username string, password string) bool {
    
    var ftp *goftp.FTP
    var err error	
	if ftp, err = goftp.Connect(SC.AUTH_SERVER); err != nil {
            panic(err.Error())
        }
    defer ftp.Close()

    if err = ftp.Login(username, password); err != nil {
            return true
        }
    return false
}

func Login(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	//decoder := json.NewDecoder(r.Body)
	Raw_User := model.Electorate_Login{}
	//err := decoder.Decode(&Raw_User)
    Raw_User.Username=r.FormValue("Username")
    Raw_User.Password=r.FormValue("Password")
    fmt.Println("In login",Raw_User.Username)
    //if err!=nil {
/*        t, _ := template.ParseFiles("/media/harpreet/winstuff/ACA/Best_Faculty_Voting/src/views/auth.html")
        t.Execute(w, &map[string]string{"Username":"Credentials Not Added"} )
        //fmt.Println(w)
        return*/
    //    http.Error(w,"Internal_Server_Error",500)
  //  }
    Category,str_err:=Raw_User.Validate()
    if str_err!=""{
        http.Error(w,str_err,401)
        return
    }
/*    guard := login_from_server(Raw_User.Username,Raw_User.Password)
    if guard {
    	http.Error(w,"Wrong Password",401)
        return
    }*/
//fmt.Println("Logged in from remote server")
    cookie := model.Bake(Raw_User.Username + "@" + Category);
//fmt.Println("Got cookie :",cookie)
   // t, _ := template.ParseFiles("/media/harpreet/winstuff/ACA/Best_Faculty_Voting/src/views/auth.html")
//fmt.Println("Parsed :",cookie)
//    w.Header().Set("Set-Cookie", cookie)
    http.SetCookie(w,&http.Cookie{Name:"ACAVote",Value:cookie})
    http.Redirect(w,r,"/paper",302)
  
   // t.Execute(w, &map[string]string{"Username":""})
}

func Logout(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	//decoder := json.NewDecoder(r.Body)
	//var cookie string;
    var cookie string
    cookies := r.Cookies()
    for _,value:= range cookies{
        if value.Name=="ACAVote" {
            cookie = value.Value
            break;
        }
    }
	//err := decoder.Decode(&cookie)
/*    if err!=nil {
        http.Error(w,"Internal_Server_Error",500)
        return
    }*/
    model.Burn(cookie);
        http.Redirect(w,r,"/",302)
    
}

func Index(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
    var cookie string
    cookies := r.Cookies()
    for _,value:= range cookies{
        if value.Name=="ACAVote" {
            cookie = value.Value
            break;
        }
    }
    guard := model.Check_logged_in(cookie)
    if guard {
        t, _ := template.ParseFiles("/media/harpreet/winstuff/ACA/Best_Faculty_Voting/src/views/auth.html")
        t.Execute(w, &map[string]string{"Username":""} )
        return
    }else{
        http.Redirect(w,r,"/paper",302)
    }
}